#ifndef UNITSLISTMODEL_H
#define UNITSLISTMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/ResourceModel/GameResource.h"

class UnitsListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum Columns
    {
        COLUMN_ICON,
        COLUMN_NAME,

        COLUMN_COUNT
    };
    explicit UnitsListModel(QObject *parent = nullptr);

signals:


    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    void init(const QString& path);
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QString path() const;

    QSharedPointer<DBFModel> model() const;
    void setModel(const QSharedPointer<DBFModel> &model);

    QSharedPointer<ResourceModel> rModel() const;
    void setRModel(const QSharedPointer<ResourceModel> &rModel);

    QSharedPointer<Gunit> unitByRow(int row)
    {
        return m_units[row];
    }
private:
    QList<QSharedPointer<Gunit>> m_units;
    QList<QImage> m_icons;
    QSharedPointer<DBFModel> m_model;
    QSharedPointer<ResourceModel> m_rModel;
    QString m_path;
};

#endif // UNITSLISTMODEL_H
