#include "UnitsListModel.h"

UnitsListModel::UnitsListModel(QObject *parent) : QAbstractTableModel(parent)
{
    m_model = QSharedPointer<DBFModel>(new DBFModel());
    m_rModel = QSharedPointer<ResourceModel>(new ResourceModel());
}

int UnitsListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_units.count();
}

int UnitsListModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return COLUMN_COUNT;
}

#include <QElapsedTimer>
void UnitsListModel::init(const QString &path)
{
    beginResetModel();
    m_path = path;
    QElapsedTimer timer;
    timer.start();
    m_model->load(path + "/Globals");
    qDebug()<<"Reading dbf time = "<<timer.elapsed();
    timer.start();
    m_rModel->init(path);
    qDebug()<<"Reading resources time = "<<timer.elapsed();
    m_units.clear();
    m_icons.clear();
    timer.start();
    QVector<QSharedPointer<Gunit>> units = m_model->getList<Gunit>();
    qDebug()<<"Reading units time = "<<timer.elapsed();
    qDebug()<<"Units count = "<<units.count();
    timer.start();
    int count = 0;
    foreach(QSharedPointer<Gunit> unit, units)
    {
        m_units.append(unit);
        QString unitIconId = unit->base_unit.getKey() + "FACE";
        if (unit->base_unit.value == nullptr)
            unitIconId = unit->unit_id + "FACE";
        QList<QImage> images = m_rModel->getFramesById("Faces", unitIconId);
        if (images.count() > 0)
            m_icons.append(images.first());
        else
            m_icons.append(QImage());
    }
qDebug()<<"Filling icons time = "<<timer.elapsed();
    endResetModel();
}

QVariant UnitsListModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole && index.column() == COLUMN_NAME)
        return m_units[index.row()]->name_txt->text;
    if (role == Qt::DecorationRole && index.column() == COLUMN_ICON)
        return m_icons[index.row()];
    return QVariant();
}

QVariant UnitsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal)
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        switch(section)
        {
        case COLUMN_ICON: return "Icon";
        case COLUMN_NAME: return "Name";
        }
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}

QString UnitsListModel::path() const
{
    return m_path;
}

QSharedPointer<DBFModel> UnitsListModel::model() const
{
    return m_model;
}

void UnitsListModel::setModel(const QSharedPointer<DBFModel> &model)
{
    m_model = model;
}

QSharedPointer<ResourceModel> UnitsListModel::rModel() const
{
    return m_rModel;
}

void UnitsListModel::setRModel(const QSharedPointer<ResourceModel> &rModel)
{
    m_rModel = rModel;
}
