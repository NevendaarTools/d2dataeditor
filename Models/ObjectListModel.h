#ifndef OBJECTLISTMODEL_H
#define OBJECTLISTMODEL_H

#include <QObject>

#include <QObject>
#include <QAbstractTableModel>
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/ResourceModel/GameResource.h"

struct RowData
{
    QString name;
    QString id;
    QString type;
    QImage icon;
};
Q_DECLARE_METATYPE(RowData)

class ObjectListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum Columns
    {
        COLUMN_ICON,
        COLUMN_NAME,

        COLUMN_COUNT
    };

    enum Roles
    {
        ROW_DATA_ROLE = Qt::UserRole + 1
    };
    explicit ObjectListModel(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    QVector<RowData> items() const;
    void setItems(const QVector<RowData> &items);

signals:
private:
    QVector<RowData> m_items;
};

#endif // OBJECTLISTMODEL_H
