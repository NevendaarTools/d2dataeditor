#include "ObjectListModel.h"

ObjectListModel::ObjectListModel(QObject *parent) : QAbstractTableModel(parent)
{

}

QVariant ObjectListModel::data(const QModelIndex &index, int role) const
{
    if ((role == Qt::DisplayRole || role == Qt::ToolTipRole) && index.column() == COLUMN_NAME)
        return m_items[index.row()].name;
    if (role == Qt::DecorationRole && index.column() == COLUMN_ICON)
        return m_items[index.row()].icon;
    if (role == ROW_DATA_ROLE)
    {
        return QVariant::fromValue(m_items[index.row()]);
    }
    return QVariant();
}

QVariant ObjectListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal)
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        switch(section)
        {
        case COLUMN_ICON: return "Icon";
        case COLUMN_NAME: return "Name";
        }
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}

int ObjectListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_items.count();
}

int ObjectListModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return COLUMN_COUNT;
}

QVector<RowData> ObjectListModel::items() const
{
    return m_items;
}

void ObjectListModel::setItems(const QVector<RowData> &items)
{
    beginResetModel();
    m_items = items;
    endResetModel();
}
