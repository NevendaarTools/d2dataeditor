#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/ResourceModel/GameResource.h"
#include <QElapsedTimer>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      m_model(new UnitsListModel(this))
{
    ui->setupUi(this);
    ui->tableView->setModel(m_model);
    QHeaderView *verticalHeader = ui->tableView->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(90);
    QHeaderView *horisontalHeader = ui->tableView->horizontalHeader();
    horisontalHeader->setSectionResizeMode(QHeaderView::Fixed);
    horisontalHeader->setDefaultSectionSize(160);
    QElapsedTimer timer;
    timer.start();
    qDebug()<<m_model->path() << " time = "<<timer.elapsed();
//    timer.start();
//    m_model->init("E:/Games/Disciples_clear/Rise Of The Elves/");
//    qDebug()<<m_model->path() << " time = "<<timer.elapsed();
//    timer.start();
//    m_model->init("E:/Games/Disciples_dwarfs");
//    qDebug()<<m_model->path() << " time = "<<timer.elapsed();
//    timer.start();
//    m_model->init("E:/Games/Disciples II Motlin");
//    qDebug()<<m_model->path() << " time = "<<timer.elapsed();

    ui->widget->setModel(m_model->model());
    ui->widget->setRModel(m_model->rModel());

    connect(ui->tableView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(onSelectionChanged()));
    m_model->init("E:/Games/Semga_actual/");
}

MainWindow::~MainWindow()
{
    //m_model->model()->save("c:/tmp/d2model");
    delete ui;
}

void MainWindow::onSelectionChanged()
{
    QList<QModelIndex> selected = ui->tableView->selectionModel()->selectedRows();
    QString text = "";
    QList<QSharedPointer<Gunit>> units;
    for(const QModelIndex& index : selected)
    {
        units << m_model->unitByRow(index.row());
    }
    ui->widget->setUnits(units);
}


void MainWindow::on_actionSave_triggered()
{
    m_model->model()->save("E:/Games/Semga_actual/Globals");
}

void MainWindow::on_actionOpen_triggered()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    if (!dir.isEmpty())
        m_model->init(dir);
//    m_model->init("E:/Games/Semga_actual/");
}
