#ifndef INTLINKEDITOR_H
#define INTLINKEDITOR_H
#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QSet>
#include <QHBoxLayout>
#include "FieldBinder.h"
#include <QComboBox>
#include <QAbstractItemView>
#include <QDebug>

template<class classT, class linkClassT>
class IntLinkBinder : public FieldBinder
{
public:
    IntLinkBinder(const QList<QSharedPointer<classT>>& items,
              const QList<QSharedPointer<linkClassT>>& variants,
              const QString& name,
              std::function<int(QSharedPointer<classT>)> get,
              std::function<void(QSharedPointer<classT>, QSharedPointer<linkClassT>)>set,
              std::function<QString(QSharedPointer<linkClassT>)> getVariantName)
        : m_items (items), m_variants(variants),
          m_getFunction(get),
          m_setFunction(set),
          m_getNameFunction(getVariantName)
    {

        m_widget = new QWidget();
        int w = name.isEmpty()?135:180;
        m_widget->setFixedWidth(w);
        m_widget->setLayout(new QHBoxLayout());
        m_widget->setContentsMargins(0,0,0,0);
        m_widget->layout()->setMargin(0);
        if (!name.isEmpty())
        {
            QLabel * nameLabel = new QLabel(name);
            nameLabel->setFixedWidth(45);
            m_widget->layout()->addWidget(nameLabel);
        }
        m_edit = new QComboBox();
        m_edit->setEditable(true);
        m_edit->setFixedWidth(128);
        m_widget->layout()->addWidget(m_edit);
        fill();
    }

    ~IntLinkBinder()
    {
        m_widget->deleteLater();
    }

    virtual void fill() override
    {
        QSet<int> dataValues;
        foreach(QSharedPointer<classT> item, m_items)
        {
            if (item.isNull())
                continue;
            dataValues.insert(m_getFunction(item));
        }
        m_edit->clear();
        foreach(QSharedPointer<linkClassT> item, m_variants)
        {
            m_edit->addItem(m_getNameFunction(item));
        }
        updateItemsW();
        if (dataValues.count() == 0)
        {
            m_edit->setPlaceholderText("no data");
            m_edit->setCurrentText("");
            return;
        }
        if (dataValues.count() == 1)
        {
            for(int i = 0; i < m_variants.count(); ++i)
            {
                if (m_variants[i]->id == dataValues.values().first())
                {
                    m_edit->setCurrentIndex(i);
                    return;
                }
            }
        }
        m_edit->setPlaceholderText("***");
        m_edit->setCurrentText("");
    }

    void updateItemsW()
    {
        //determinge the maximum width required to display all names in full
        int max_width = 0;
        QFontMetrics fm(m_edit->font());
        for(int x = 0; x < m_edit->count(); ++x)
        {
            int width = fm.width(m_edit->itemText(x));
            if(width > max_width)
                max_width = width;
        }
        if(m_edit->view()->minimumWidth() < max_width)
        {
            // add scrollbar width and margin
            max_width += m_edit->style()->pixelMetric(QStyle::PM_ScrollBarExtent);
            max_width += m_edit->view()->autoScrollMargin();
            // set the minimum width of the combobox drop down list
            m_edit->view()->setMinimumWidth(max_width);
        }
    }

    virtual void commit() override
    {
        if (!m_edit->currentText().isEmpty())
        {
            QSharedPointer<linkClassT> value = m_variants.at(m_edit->currentIndex());
            foreach(QSharedPointer<classT> item, m_items)
            {
                if (item.isNull())
                    continue;
                m_setFunction(item, value);
            }
        }
    }
    QWidget *widget() const
    {
        return m_widget;
    }

protected:
    QList<QSharedPointer<classT>> m_items;
    QList<QSharedPointer<linkClassT>> m_variants;
    QWidget * m_widget;
    QComboBox * m_edit;
    QLabel * m_label;

    std::function<int(QSharedPointer<classT>)> m_getFunction;
    std::function<void(QSharedPointer<classT>, QSharedPointer<linkClassT>)> m_setFunction;
    std::function<QString(QSharedPointer<linkClassT>)> m_getNameFunction;
};
#endif // INTLINKEDITOR_H
