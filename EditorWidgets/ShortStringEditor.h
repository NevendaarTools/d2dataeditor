#ifndef SHORTSTRINGEDITOR_H
#define SHORTSTRINGEDITOR_H
#include <QLineEdit>
#include <QLabel>
#include <QSet>
#include <QHBoxLayout>
#include "FieldBinder.h"

template<class classT>
class ShortStringBinder : public FieldBinder
{
public:
    ShortStringBinder(const QList<QSharedPointer<classT>>& items,
              const QString& name,
               std::function<QString(QSharedPointer<classT>)> get,
              std::function<void(QSharedPointer<classT>, QString)>set)
        : m_items (items), m_getFunction(get), m_setFunction(set)
    {

        m_edit = new QLineEdit();
        m_edit->setFixedWidth(260);
        m_edit->setText(name);
        m_widget = new QWidget();
        int w = name.isEmpty()?270:330;
        m_widget->setFixedWidth(w);
        m_widget->setLayout(new QHBoxLayout());
        m_widget->setContentsMargins(0,0,0,0);
        m_widget->layout()->setMargin(0);
        if (!name.isEmpty())
        {
            QLabel * nameLabel = new QLabel(name);
            nameLabel->setFixedWidth(60);
            m_widget->layout()->addWidget(nameLabel);
        }
        m_widget->layout()->addWidget(m_edit);
        fill();
    }

    ~ShortStringBinder()
    {
        m_widget->deleteLater();
    }

    virtual void fill() override
    {
        QSet<QString> dataValues;
        foreach(QSharedPointer<classT> item, m_items)
        {
            if (item.isNull())
                continue;
            dataValues.insert(m_getFunction(item));
        }
        if (dataValues.count() == 0)
        {
            m_edit->setPlaceholderText("no data");
            m_edit->setText("");
            return;
        }
        if (dataValues.count() == 1)
        {
            m_edit->setText(dataValues.values().first());
            return;
        }
        m_edit->setPlaceholderText("***");
        m_edit->setText("");
    }
    virtual void commit() override
    {
        if (!m_edit->text().isEmpty())
        {
            checkValue();
            QString value = m_edit->text();
            foreach(QSharedPointer<classT> item, m_items)
            {
                if (item.isNull())
                    continue;
                m_setFunction(item, value);
            }
        }
    }
    void checkValue()
    {
        QString value = m_edit->text();

        if (value.length() > 256)
            m_edit->setText(value.left(256));
    }
    QWidget *widget() const
    {
        return m_widget;
    }

protected:
    QList<QSharedPointer<classT>> m_items;
    QWidget * m_widget;
    QLineEdit * m_edit;

    std::function<QString(QSharedPointer<classT>)> m_getFunction;
    std::function<void(QSharedPointer<classT>, QString)> m_setFunction;
};

#endif // SHORTSTRINGEDITOR_H
