#ifndef GATTACKEDITOR_H
#define GATTACKEDITOR_H

#include <QWidget>
#include <QWidget>
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/ResourceModel//GameResource.h"
#include <QLabel>
#include <QSpinBox>
#include <QGridLayout>
#include <QPushButton>
#include <QTableView>
#include "EditorWidgets/IntFieldEditor.h"
#include "EditorWidgets/BoolFieldEditor.h"
#include "EditorWidgets/IntLinkEditor.h"
#include "Models/ObjectListModel.h"

class GAttackEditor : public QWidget
{
    Q_OBJECT
public:
    explicit GAttackEditor(QWidget *parent = nullptr);

    QSharedPointer<ResourceModel> rModel() const;
    void setRModel(const QSharedPointer<ResourceModel> &rModel);

    QSharedPointer<DBFModel> model() const;
    void setModel(const QSharedPointer<DBFModel> &model);

    QList<QSharedPointer<Gattack> > items() const;
    void setItems(const QList<QSharedPointer<Gattack> > &attacks, bool withAlt = true);
    void fill();
signals:
public slots:
    void onApply();
protected:
    void addField(QGridLayout *grid, FieldBinder * binder, int x, int y);
private:
    QGridLayout * m_grid;
    QSharedPointer<DBFModel> m_dbfmodel;
    QSharedPointer<ResourceModel> m_rModel;
    QList<QSharedPointer<Gattack>> m_items;

    QList<QSharedPointer<FieldBinder>> m_controls;

    QPushButton * m_applyButton;

    QTableView * m_applyedView;
    GAttackEditor * m_altAttack;
    ObjectListModel * m_model;
};

#endif // GATTACKEDITOR_H
