#include "GAttackEditor.h"
#include <QHeaderView>

GAttackEditor::GAttackEditor(QWidget *parent) : QWidget(parent)
{
    m_grid = new QGridLayout();
    setLayout(m_grid);
    m_applyButton = new QPushButton("Apply");
    connect(m_applyButton, &QPushButton::clicked, this, &GAttackEditor::onApply);
    m_applyedView = new QTableView();
    m_model = new ObjectListModel(this);
    m_applyedView->setModel(m_model);
    m_applyedView->verticalHeader()->hide();
    m_applyedView->horizontalHeader()->setStretchLastSection(true);
    m_applyedView->horizontalHeader()->resizeSection(0, 40);
    m_applyedView->horizontalHeader()->hide();
    m_altAttack = nullptr;
}

QSharedPointer<ResourceModel> GAttackEditor::rModel() const
{
    return m_rModel;
}

void GAttackEditor::setRModel(const QSharedPointer<ResourceModel> &rModel)
{
    m_rModel = rModel;
}

QSharedPointer<DBFModel> GAttackEditor::model() const
{
    return m_dbfmodel;
}

void GAttackEditor::setModel(const QSharedPointer<DBFModel> &model)
{
    m_dbfmodel = model;
}

QList<QSharedPointer<Gattack> > GAttackEditor::items() const
{
    return m_items;
}

void GAttackEditor::setItems(const QList<QSharedPointer<Gattack> > &attacks, bool withAlt)
{
    m_items = attacks;
    m_controls.clear();
    int startIndex = 1;
    int index = startIndex;
    index = startIndex;

    QList<QSharedPointer<LattS>> sourceVarians = m_dbfmodel->getList<LattS>().toList();
    addField(m_grid,
             new IntLinkBinder<Gattack, LattS>(m_items, sourceVarians, "source",
             [](QSharedPointer<Gattack> attack) -> int {return attack->source.key;},
             [](QSharedPointer<Gattack> attack, QSharedPointer<LattS> val){attack->source.value = val; attack->source.key = val->id;},
             [](QSharedPointer<LattS> attSource) ->QString {return attSource->text;}), startIndex++, 0);
    QList<QSharedPointer<LattC>> classVarians = m_dbfmodel->getList<LattC>().toList();
    addField(m_grid,
             new IntLinkBinder<Gattack, LattC>(m_items, classVarians, "class",
             [](QSharedPointer<Gattack> attack) -> int {return attack->clas.key;},
             [](QSharedPointer<Gattack> attack, QSharedPointer<LattC> val){attack->clas.value = val; attack->clas.key = val->id;},
             [](QSharedPointer<LattC> attSource) ->QString {return attSource->text;}), startIndex++, 0);
    QList<QSharedPointer<LAttR>> reachVarians = m_dbfmodel->getList<LAttR>().toList();
    addField(m_grid,
             new IntLinkBinder<Gattack, LAttR>(m_items, reachVarians, "reach",
             [](QSharedPointer<Gattack> attack) -> int {return attack->reach.key;},
             [](QSharedPointer<Gattack> attack, QSharedPointer<LAttR> val){attack->reach.value = val; attack->reach.key = val->id;},
             [](QSharedPointer<LAttR> attReach) ->QString {return attReach->text;}), startIndex++, 0);

    addField(m_grid,
             new IntBinder<Gattack>(m_items, "initiative", 1, 150,
             [](QSharedPointer<Gattack> attack) ->int {return attack->initiative;},
             [](QSharedPointer<Gattack> attack, int val){attack->initiative = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gattack>(m_items, "accuracy", 0, 100,
             [](QSharedPointer<Gattack> attack) ->int {return attack->power;},
             [](QSharedPointer<Gattack> attack, int val){attack->power = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gattack>(m_items, "heal", 0, 99999,
             [](QSharedPointer<Gattack> attack) ->int {return attack->qty_heal;},
             [](QSharedPointer<Gattack> attack, int val){attack->qty_heal = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gattack>(m_items, "damage", 0, 99999,
             [](QSharedPointer<Gattack> attack) ->int {return attack->qty_dam;},
             [](QSharedPointer<Gattack> attack, int val){attack->qty_dam = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gattack>(m_items, "level", 0, 99,
             [](QSharedPointer<Gattack> attack) ->int {return attack->level;},
             [](QSharedPointer<Gattack> attack, int val){attack->level = val;}), startIndex++, 0);
    addField(m_grid,
             new BoolBinder<Gattack>(m_items, "infinite",
             [](QSharedPointer<Gattack> attack) ->bool {return attack->infinite;},
             [](QSharedPointer<Gattack> attack, bool val){attack->infinite = val;}), startIndex++, 0);
    addField(m_grid,
             new BoolBinder<Gattack>(m_items, "crit",
             [](QSharedPointer<Gattack> attack) ->bool {return attack->crit_hit;},
             [](QSharedPointer<Gattack> attack, bool val){attack->crit_hit = val;}), startIndex++, 0);
    QVector<QSharedPointer<Gunit>> units = m_dbfmodel->getList<Gunit>();
    QVector<QSharedPointer<GItem>> items = m_dbfmodel->getList<GItem>();
    QVector<RowData> modelData;
    QSet<QString> tmp;

    foreach(QSharedPointer<Gunit> unit, units)
    {
        foreach(QSharedPointer<Gattack> attack, attacks)
        {
            if (attack.isNull())
                continue;
            if (unit.isNull())
                continue;
            if (unit->attack_id.getKey() == attack->key() ||
                unit->attack2_id.getKey() == attack->key())
            {
                if (tmp.contains(unit->key()))
                    continue;
                RowData data;
                QString unitIconId = unit->base_unit.getKey() + "FACE";
                if (unit->base_unit.value == nullptr)
                    unitIconId = unit->unit_id + "FACE";
                QList<QImage> images = m_rModel->getFramesById("Faces", unitIconId);
                if (images.count() > 0)
                    data.icon = (images.first().scaled(QSize(20,20),Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
                data.id = unit->key();
                data.name = unit->name_txt->text.trimmed() + "-" + unit->key();
                data.type = Gunit::typeName();
                modelData.append(data);
                tmp.insert(unit->key());
            }
        }
    }
    tmp.clear();
    int attackCount = 0;
    QList<QSharedPointer<Gattack>> altAttacks;
    foreach(QSharedPointer<Gattack> attack, attacks)
    {
        if (attack.isNull())
            continue;
        if (!attack->alt_attack.value.isNull() && withAlt)
            altAttacks<<attack->alt_attack.value;
        attackCount++;
        foreach(QSharedPointer<GItem> item, items)
        {
            if (item.isNull())
                continue;
            if (item->attack_id.getKey().toUpper()!= attack->key().toUpper())
                continue;
            if (tmp.contains(item->key()))
                continue;
            RowData data;
            QList<QImage> images = m_rModel->getFramesById("IconItem", item->key().toUpper());
            if (images.count() > 0)
                data.icon = (images.first().scaled(QSize(20,20),Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
            data.id = item->key();
            data.name = item->name_txt.value->text;
            data.type = GItem::typeName();
            modelData.append(data);
            tmp.insert(item->key());
        }
    }
    m_model->setItems(modelData);

    addField(m_grid,new SimpleShowTextBinder("Applyes to:"), startIndex++, 0);

    m_grid->addWidget(m_applyedView, startIndex++, 0);
    if (withAlt && altAttacks.count() > 0)
    {
        if (m_altAttack)
            m_altAttack->deleteLater();
        m_altAttack = new GAttackEditor(this);
        m_altAttack->setModel(m_dbfmodel);
        m_altAttack->setRModel(m_rModel);
        m_grid->addWidget(m_altAttack, startIndex, 0);
        m_altAttack->setItems(altAttacks, false);
    }
    if (attackCount == 0)
        setEnabled(false);
}

void GAttackEditor::fill()
{
    foreach(QSharedPointer<FieldBinder> control, m_controls)
        control->fill();
}

void GAttackEditor::onApply()
{
    foreach(QSharedPointer<FieldBinder> control, m_controls)
        control->commit();
    for(int i = 0; i < m_items.count(); ++i)
    {
        m_dbfmodel->update(m_items[i]);
    }
    fill();
}

void GAttackEditor::addField(QGridLayout *grid, FieldBinder *binder, int x, int y)
{
    m_controls.append(QSharedPointer<FieldBinder>(binder));
    grid->addWidget(binder->widget(), x, y);
}
