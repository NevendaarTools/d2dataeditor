#ifndef FIELDBINDER_H
#define FIELDBINDER_H
#include <QWidget>

class FieldBinder
{
public:
    virtual ~FieldBinder(){}
    virtual void fill() = 0;
    virtual void commit() = 0;
    virtual QWidget *widget() const = 0;
};
#endif // FIELDBINDER_H
