#include "GUnitEditor.h"
#include "ShortStringEditor.h"
#include "LongStringEditor.h"

GUnitEditor::GUnitEditor(QWidget *parent) : QWidget(parent)
{
    m_grid = new QGridLayout();
    setLayout(m_grid);
    m_applyButton = new QPushButton("Apply");
    connect(m_applyButton, &QPushButton::clicked, this, &GUnitEditor::onApply);
}

void GUnitEditor::addField(QGridLayout *grid, FieldBinder *binder, int x, int y)
{
    m_controls.append(QSharedPointer<FieldBinder>(binder));
    grid->addWidget(binder->widget(), x, y);
}

void GUnitEditor::addField(QGridLayout *grid, FieldBinder *binder, int x, int y, int w, int h)
{
    m_controls.append(QSharedPointer<FieldBinder>(binder));
    grid->addWidget(binder->widget(), x, y, h, w);
}

QList<QSharedPointer<Gunit> > GUnitEditor::units() const
{
    return m_units;
}

void GUnitEditor::setUnits(const QList<QSharedPointer<Gunit> > &units)
{
    m_controls.clear();
    if (m_attack1Editor)
        m_attack1Editor->deleteLater();
    if (m_attack2Editor)
        m_attack2Editor->deleteLater();
    m_units = units;
    int startIndex = 1;
    int index = startIndex;
    index = startIndex;

    addField(m_grid,
             new ShortStringBinder<Gunit>(m_units, "Name",
             [](QSharedPointer<Gunit> unit) ->QString {return unit->name_txt->text.trimmed();},
             [](QSharedPointer<Gunit> unit, QString val){unit->name_txt->text = val;}), startIndex++, 0,3,1);
    addField(m_grid,
             new LongStringBinder<Gunit>(m_units, "Description",
             [](QSharedPointer<Gunit> unit) ->QString {return unit->desc_txt->text.trimmed();},
             [](QSharedPointer<Gunit> unit, QString val){unit->desc_txt->text = val;}), startIndex++, 0,3,1);

    QList<QSharedPointer<LSubRace>> subRaceVarians = m_dbfmodel->getList<LSubRace>().toList();
    addField(m_grid,
             new IntLinkBinder<Gunit, LSubRace>(m_units, subRaceVarians, "Subrace",
             [](QSharedPointer<Gunit> attack) -> int {return attack->subrace.key;},
             [](QSharedPointer<Gunit> attack, QSharedPointer<LSubRace> val){attack->subrace.value = val; attack->subrace.key = val->id;},
             [](QSharedPointer<LSubRace> subRace) ->QString {return subRace->text;}), startIndex++, 0);

    QList<QSharedPointer<LunitB>> branchVarians = m_dbfmodel->getList<LunitB>().toList();
    addField(m_grid,
             new IntLinkBinder<Gunit, LunitB>(m_units, branchVarians, "Branch",
             [](QSharedPointer<Gunit> attack) -> int {return attack->branch.key;},
             [](QSharedPointer<Gunit> attack, QSharedPointer<LunitB> val){attack->branch.value = val; attack->branch.key = val->id;},
             [](QSharedPointer<LunitB> subRace) ->QString {return subRace->text;}), startIndex++, 0);
    addField(m_grid,
             new BoolBinder<Gunit>(m_units, "small",
             [](QSharedPointer<Gunit> unit) ->bool {return unit->size_small;},
             [](QSharedPointer<Gunit> unit, bool val){unit->size_small = val;}), startIndex++, 0);
    addField(m_grid,
             new BoolBinder<Gunit>(m_units, "sex:male",
             [](QSharedPointer<Gunit> unit) ->bool {return unit->sex_m;},
             [](QSharedPointer<Gunit> unit, bool val){unit->sex_m = val;}), startIndex++, 0);
    addField(m_grid,
             new BoolBinder<Gunit>(m_units, "Water only",
             [](QSharedPointer<Gunit> unit) ->bool {return unit->water_only;},
             [](QSharedPointer<Gunit> unit, bool val){unit->water_only = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "Level", 1, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->level;},
             [](QSharedPointer<Gunit> unit, int val){unit->level = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "Dyn upgr level", 1, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg_lv;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg_lv = val;}), startIndex, 0);
    addField(m_grid, new SimpleShowTextBinder("Upgrade\nbefore"), startIndex, 1);
    addField(m_grid,new SimpleShowTextBinder("Upgrade\nafter"), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "XP killed", 1, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->xp_killed;},
             [](QSharedPointer<Gunit> unit, int val){unit->xp_killed = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->xp_killed;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->xp_killed = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->xp_killed;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->xp_killed = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "XP next", 1, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->xp_next;},
             [](QSharedPointer<Gunit> unit, int val){unit->xp_next = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->xp_next;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->xp_next = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->xp_next;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->xp_next = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "health", 1, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->hit_point;},
             [](QSharedPointer<Gunit> unit, int val){unit->hit_point = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->hit_point;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->hit_point = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->hit_point;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->hit_point = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "armor", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->armor;},
             [](QSharedPointer<Gunit> unit, int val){unit->armor = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->armor;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->armor = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->armor;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->armor = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "regen", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->regen;},
             [](QSharedPointer<Gunit> unit, int val){unit->regen = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->regen;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->regen = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->regen;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->regen = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new BoolBinder<Gunit>(m_units, "Attack twice",
             [](QSharedPointer<Gunit> unit) ->bool {return unit->atck_twice;},
             [](QSharedPointer<Gunit> unit, bool val){unit->atck_twice = val;}), startIndex++, 0);
//    addField(m_grid,
//             new IntBinder<Gunit>(m_units, "accuracy", 0, 100,
//             [](QSharedPointer<Gunit> unit) ->int {return unit->attack_id.m_value->power;},
//             [](QSharedPointer<Gunit> unit, int val){unit->attack_id.m_value->power = val;}), startIndex, 0);
    addField(m_grid,new SimpleShowTextBinder("accuracy (managed by attack1)"), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->power;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->power = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->power;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->power = val;}), startIndex, 2);
    startIndex++;
//    addField(m_grid,
//             new IntBinder<Gunit>(m_units, "initiative", 0, 99,
//             [](QSharedPointer<Gunit> unit) ->int {return unit->attack_id.m_value->initiative;},
//             [](QSharedPointer<Gunit> unit, int val){unit->attack_id.m_value->initiative = val;}), startIndex, 0);
    addField(m_grid,new SimpleShowTextBinder("initiative (managed by attack1)"), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->initiative;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->initiative = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->initiative;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->initiative = val;}), startIndex, 2);
    startIndex++;
//    addField(m_grid,
//             new IntBinder<Gunit>(m_units, "heal", 0, 99999,
//             [](QSharedPointer<Gunit> unit) ->int {return unit->attack_id.m_value->qty_heal;},
//             [](QSharedPointer<Gunit> unit, int val){unit->attack_id.m_value->qty_heal = val;}), startIndex, 0);
        addField(m_grid,new SimpleShowTextBinder("heal (managed by attack1)"), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->heal;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->heal = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->heal;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->heal = val;}), startIndex, 2);
    startIndex++;
//    addField(m_grid,
//             new IntBinder<Gunit>(m_units, "power", 0, 99999,
//             [](QSharedPointer<Gunit> unit) ->int {return unit->attack_id.m_value->qty_dam;},
//             [](QSharedPointer<Gunit> unit, int val){unit->attack_id.m_value->qty_dam = val;}), startIndex, 0);
    addField(m_grid,new SimpleShowTextBinder("damage (managed by attack1)"), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 1, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->damage;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->damage = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 1, 99999,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->damage;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->damage = val;}), startIndex, 2);
    startIndex++;
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "move", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->move;},
             [](QSharedPointer<Gunit> unit, int val){unit->move = val;}), startIndex, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg1.value->move;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg1.value->move = val;}), startIndex, 1);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->dyn_upg2.value->move;},
             [](QSharedPointer<Gunit> unit, int val){unit->dyn_upg2.value->move = val;}), startIndex, 2);
    startIndex++;

    addField(m_grid,
             new IntBinder<Gunit>(m_units, "scout range", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->scout;},
             [](QSharedPointer<Gunit> unit, int val){unit->scout = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "leadership", 0, 5,
             [](QSharedPointer<Gunit> unit) ->int {return unit->leadership;},
             [](QSharedPointer<Gunit> unit, int val){unit->leadership = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "negotiate", 0, 100,
             [](QSharedPointer<Gunit> unit) ->int {return unit->negotiate;},
             [](QSharedPointer<Gunit> unit, int val){unit->negotiate = val;}), startIndex++, 0);
    addField(m_grid,
             new IntBinder<Gunit>(m_units, "life time\n(summon)", 0, 99,
             [](QSharedPointer<Gunit> unit) ->int {return unit->life_time;},
             [](QSharedPointer<Gunit> unit, int val){unit->life_time = val;}), startIndex++, 0);

    //
    m_attack1Editor = new GAttackEditor();
    m_attack1Editor->setModel(m_dbfmodel);
    m_attack1Editor->setRModel(m_rModel);

    m_attack2Editor = new GAttackEditor();
    m_attack2Editor->setModel(m_dbfmodel);
    m_attack2Editor->setRModel(m_rModel);

    m_grid->addWidget(m_attack1Editor, 1, 3, startIndex - index, 1);
    m_grid->addWidget(m_attack2Editor, 1, 4, startIndex - index, 1);
    QList<QSharedPointer<Gattack>> attack1Data;
    QList<QSharedPointer<Gattack>> attack2Data;

    foreach(QSharedPointer<Gunit> unit, m_units)
    {
        attack1Data.append(unit->attack_id.value);
        attack2Data.append(unit->attack2_id.value);
    }
    m_attack1Editor->setItems(attack1Data);
    m_attack2Editor->setItems(attack2Data);

    m_grid->addWidget(m_applyButton, startIndex, 3);
}

QSharedPointer<DBFModel> GUnitEditor::model() const
{
    return m_dbfmodel;
}

void GUnitEditor::setModel(const QSharedPointer<DBFModel> &model)
{
    m_dbfmodel = model;
}

QSharedPointer<ResourceModel> GUnitEditor::rModel() const
{
    return m_rModel;
}

void GUnitEditor::setRModel(const QSharedPointer<ResourceModel> &rModel)
{
    m_rModel = rModel;
}

void GUnitEditor::fill()
{
    foreach(QSharedPointer<FieldBinder> control, m_controls)
        control->fill();
    m_attack1Editor->fill();
    m_attack2Editor->fill();
}

void GUnitEditor::onApply()
{
    foreach(QSharedPointer<FieldBinder> control, m_controls)
        control->commit();
    for(int i = 0; i < m_units.count(); ++i)
    {
        m_dbfmodel->update(m_units[i]);
        m_dbfmodel->update(m_units[i]->name_txt.value);
        m_dbfmodel->update(m_units[i]->desc_txt.value);
        m_dbfmodel->update(m_units[i]->dyn_upg1.value);
        m_dbfmodel->update(m_units[i]->dyn_upg2.value);
        m_dbfmodel->update(m_units[i]->attack_id.value);
        m_dbfmodel->update(m_units[i]->attack2_id.value);
    }
    m_attack1Editor->onApply();
    m_attack2Editor->onApply();
    fill();
}

