#ifndef INTFIELDEDITOR_H
#define INTFIELDEDITOR_H

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QSet>
#include <QHBoxLayout>
#include "FieldBinder.h"

class SimpleShowTextBinder : public FieldBinder
{
public:
    SimpleShowTextBinder(const QString& text)
    {
        m_label = new QLabel(text);
    }
    ~SimpleShowTextBinder()
    {
        m_label->deleteLater();
    }
    virtual void fill(){};
    virtual void commit(){};
    virtual QWidget *widget() const {return m_label;};
protected:
    QLabel * m_label;
};

template<class classT>
class IntBinder : public FieldBinder
{
public:
    IntBinder(const QList<QSharedPointer<classT>>& items,
              const QString& name, int32_t min, int32_t max,
              std::function<int(QSharedPointer<classT>)> get,
              std::function<void(QSharedPointer<classT>, int)>set)
        : m_items (items), m_min(min), m_max(max), m_getFunction(get), m_setFunction(set)
    {

        m_widget = new QWidget();
        int w = name.isEmpty()?60:160;
        m_widget->setFixedWidth(w);
        m_widget->setLayout(new QHBoxLayout());
        m_widget->setContentsMargins(0,0,0,0);
        m_widget->layout()->setMargin(0);
        if (!name.isEmpty())
        {
            QLabel * nameLabel = new QLabel(name);
            nameLabel->setFixedWidth(100);
            m_widget->layout()->addWidget(nameLabel);
        }
        m_edit = new QLineEdit();
        m_edit->setFixedWidth(58);
        m_widget->layout()->addWidget(m_edit);
        fill();
    }

    ~IntBinder()
    {
        m_widget->deleteLater();
    }
    virtual void fill() override
    {
        QSet<int> dataValues;
        foreach(QSharedPointer<classT> item, m_items)
        {
            if (item.isNull())
                continue;
            dataValues.insert(m_getFunction(item));
        }
        if (dataValues.count() == 0)
        {
            m_edit->setPlaceholderText("no data");
            m_edit->setText("");
            return;
        }
        if (dataValues.count() == 1)
        {
            m_edit->setText(QString::number(dataValues.values().first()));
            return;
        }
        m_edit->setPlaceholderText("***");
        m_edit->setText("");
    }
    virtual void commit() override
    {
        if (!m_edit->text().isEmpty())
        {
            checkValue();
            int value = m_edit->text().toInt();
            foreach(QSharedPointer<classT> item, m_items)
            {
                if (item.isNull())
                    continue;
                m_setFunction(item, value);
            }
        }
    }
    void checkValue()
    {
        int value = m_edit->text().toInt();

        if (value < m_min)
            return m_edit->setText(QString::number(m_min));
        if (value > m_max)
            return m_edit->setText(QString::number(m_max));
    }
    QWidget *widget() const
    {
        return m_widget;
    }

protected:
    QList<QSharedPointer<classT>> m_items;
    QWidget * m_widget;
    QLineEdit * m_edit;
    QLabel * m_label;
    int m_min;
    int m_max;

    std::function<int(QSharedPointer<classT>)> m_getFunction;
    std::function<void(QSharedPointer<classT>, int)> m_setFunction;
};

#endif // INTFIELDEDITOR_H
