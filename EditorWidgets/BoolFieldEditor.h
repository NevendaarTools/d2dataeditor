#ifndef BOOLFIELDEDITOR_H
#define BOOLFIELDEDITOR_H
#include <QCheckBox>
#include <QLabel>
#include <QSet>
#include <QHBoxLayout>
#include "FieldBinder.h"

template<class classT>
class BoolBinder : public FieldBinder
{
public:
    BoolBinder(const QList<QSharedPointer<classT>>& items,
              const QString& name,
               std::function<bool(QSharedPointer<classT>)> get,
              std::function<void(QSharedPointer<classT>, bool)>set)
        : m_items (items), m_getFunction(get), m_setFunction(set)
    {

        m_edit = new QCheckBox();
        m_edit->setFixedWidth(160);
        m_edit->setText(name);
        fill();
    }

    ~BoolBinder()
    {
        m_edit->deleteLater();
    }
    virtual void fill() override
    {
        QSet<bool> dataValues;
        foreach(QSharedPointer<classT> item, m_items)
        {
            if (item.isNull())
                continue;
            dataValues.insert(m_getFunction(item));
        }
        if (dataValues.count() == 1)
        {
            m_edit->setChecked(dataValues.values().first());
            return;
        }

        m_edit->setCheckState(Qt::PartiallyChecked);
    }
    virtual void commit() override
    {
        if (m_edit->checkState() != Qt::PartiallyChecked)
        {
            foreach(QSharedPointer<classT> item, m_items)
            {
                if (item.isNull())
                    continue;
                m_setFunction(item, m_edit->isChecked());
            }
        }
    }

    QWidget *widget() const
    {
        return m_edit;
    }

protected:
    QList<QSharedPointer<classT>> m_items;
    QCheckBox * m_edit;

    std::function<bool(QSharedPointer<classT>)> m_getFunction;
    std::function<void(QSharedPointer<classT>, bool)> m_setFunction;
};

#endif // BOOLFIELDEDITOR_H
