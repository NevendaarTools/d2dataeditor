#ifndef GUNITEDITOR_H
#define GUNITEDITOR_H

#include <QWidget>
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/ResourceModel//GameResource.h"
#include <QLabel>
#include <QSpinBox>
#include <QGridLayout>
#include <QPushButton>
#include "EditorWidgets/IntFieldEditor.h"
#include "EditorWidgets/BoolFieldEditor.h"
#include "EditorWidgets/GAttackEditor.h"

class GUnitEditor : public QWidget
{
    Q_OBJECT
public:
    explicit GUnitEditor(QWidget *parent = nullptr);

    QSharedPointer<DBFModel> model() const;
    void setModel(const QSharedPointer<DBFModel> &model);

    QSharedPointer<ResourceModel> rModel() const;
    void setRModel(const QSharedPointer<ResourceModel> &rModel);

    void fill();
    void setUnits(const QList<QSharedPointer<Gunit> > &units);
    QList<QSharedPointer<Gunit> > units() const;

signals:

public slots:
    void onApply();
protected:
    void addField(QGridLayout *grid, FieldBinder * binder, int x, int y);
    void addField(QGridLayout *grid, FieldBinder *binder, int x, int y, int w, int h);
private:
    QGridLayout * m_grid;
    QSharedPointer<DBFModel> m_dbfmodel;
    QSharedPointer<ResourceModel> m_rModel;
    QList<QSharedPointer<Gunit>> m_units;

    QList<QSharedPointer<FieldBinder>> m_controls;
    GAttackEditor * m_attack1Editor = nullptr;
    GAttackEditor * m_attack2Editor = nullptr;



    QPushButton * m_applyButton;
};

#endif // GUNITEDITOR_H
